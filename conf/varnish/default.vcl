vcl 4.0;
import cookie; # Need Varnish >= 6.4.
import directors;
import std;

# This Varnish VCL has been adapted from the Four Kitchens VCL for Varnish 3.
# This VCL is for using cache tags with drupal 8. Minor changes of VCL provided
# by Jeff Geerling.

# Default backend definition. Points to Apache, normally.
# Apache is in this config on port 80.
backend default {
    .host = "web";
    .port = "80";
    .first_byte_timeout = 300s;
    .probe = {
        .request =
            "GET /health HTTP/1.0"
            "Host: web"
            "Connection: close";
        .interval = 10s;
        .timeout = 2s;
        .window = 5;
        .threshold = 3;
    }
}

# Access control list for PURGE requests.
# Here you need to put the IP address of your web server.
acl purge {
    "web";
    "drupal_cron";
}

# Configuration preparation.
sub vcl_init {
    new sites = directors.round_robin();
    sites.add_backend(default);
}

# Respond to incoming requests.
sub vcl_recv {
    # Set backend cluster.
    set req.backend_hint = sites.backend();

    # Protection against HTTPOXY CGI vulnerability.
    unset req.http.proxy;

    # Cleanup request to maximize cache hit.
    call url_cleanup;

    # Add an X-Forwarded-For header with the client IP address.
    if (req.restarts == 0) {
        if (req.http.X-Forwarded-For) {
            set req.http.X-Forwarded-For = req.http.X-Forwarded-For + ", " + client.ip;
        }
        else {
            set req.http.X-Forwarded-For = client.ip;
        }
    }

    # Only allow PURGE requests from IP addresses in the 'purge' ACL.
    if (req.method == "PURGE") {
        if (!client.ip ~ purge) {
            return (synth(405, "Not allowed."));
        }
        return (purge);
    }

    # Only allow BAN requests from IP addresses in the 'purge' ACL.
    if (req.method == "BAN") {
        # Same ACL check as above.
        if (!client.ip ~ purge) {
            return (synth(403, "Not allowed."));
        }

        # Logic for the ban, using the Cache-Tags header. For more info
        # see https://github.com/geerlingguy/drupal-vm/issues/397.
        if (req.http.Cache-Tags) {
            ban("obj.http.Cache-Tags ~ " + req.http.Cache-Tags);
        }
        # Comment out this to debug.
        #else {
        #    return (synth(403, "Cache-Tags header missing."));
        #}

        # Throw a synthetic page so the request won't go to the backend.
        return (synth(200, "Ban added."));
    }

    # Call built-in method verification sub routine.
    call vcl_req_host;
    call vcl_req_method;
    call vcl_req_authorization;

    # Pass through any administrative or AJAX-related paths.
    if (req.url ~ "^/status\.php$" ||
        req.url ~ "^/update\.php$" ||
        req.url ~ "^/admin$" ||
        req.url ~ "^/admin/.*$" ||
        req.url ~ "^/flag/.*$" ||
        req.url ~ "^.*/ajax/.*$" ||
        req.url ~ "^.*/ahah/.*$") {
            return (pass);
    }

    # Removing cookies for static content so Varnish caches these files.
    # If not coming from the private file system.
    if (req.url ~ "(?i)\.(asc|css|csv|dat|doc|eof|eot|f4v|flv|gif|ico|js|jpeg|jpg|mov|mp3|mp4|pdf|png|ppt|svg|swf|tgz|ttf|txt|woff|xls)(\?[a-z0-9]+)?$" && req.url !~ "/system/files") {
        unset req.http.Cookie;
    }

    cookie.parse(req.http.Cookie);
    # If there is a session or NO_CACHE cookie.
    if (cookie.get_re("(SESS[a-z0-9]+|SSESS[a-z0-9]+|NO_CACHE)")) {
        # Do not cache the page. Pass it on to the backend directly.
        return (pass);
    }

    # Need to end with a return to not call builtin VCL logic to ensure even
    # with cookies the page will be cached.
    return (hash);
}

# Instruct Varnish what to do in the case of certain backend responses (beresp).
sub vcl_backend_response {
    # Set ban-lurker friendly custom headers.
    set beresp.http.X-Url = bereq.url;
    set beresp.http.X-Host = bereq.http.host;

    # Cache 404s, 301s, at 500s with a short lifetime to protect the backend.
    if (beresp.status == 404 || beresp.status == 301 || beresp.status == 500) {
        set beresp.ttl = 10m;
    }

    # Enable streaming directly to backend for BigPipe responses.
    if (beresp.http.Surrogate-Control ~ "BigPipe/1.0") {
        set beresp.do_stream = true;
        set beresp.ttl = 0s;
    }

    # Don't allow static files to set cookies.
    # (?i) denotes case insensitive in PCRE (perl compatible regular expressions).
    # This list of extensions appears twice, once here and again in vcl_recv so
    # make sure you edit both and keep them equal.
    if (bereq.url ~ "(?i)\.(asc|css|csv|dat|doc|eof|eot|f4v|flv|gif|ico|js|jpeg|jpg|mov|mp3|mp4|pdf|png|ppt|svg|swf|tgz|ttf|txt|woff|xls)(\?[a-z0-9]+)?$") {
        unset beresp.http.set-cookie;
    }

    # Allow items to remain in cache up to 6 hours past their cache expiration.
    set beresp.grace = 6h;
}

# Set a header to track a cache HITs and MISSes.
sub vcl_deliver {
    # Remove ban-lurker friendly custom headers when delivering to client.
    unset resp.http.X-Url;
    unset resp.http.X-Host;

    # Comment these for easier Drupal cache tag debugging in development.
    unset resp.http.Cache-Tags;
    unset resp.http.Purge-Cache-Tags;
    unset resp.http.X-Drupal-Cache-Contexts;

    if (obj.hits > 0) {
        set resp.http.X-Cache = "HIT";
        set resp.http.X-Cache-Hits = obj.hits;
    }
    else {
        set resp.http.X-Cache = "MISS";
    }
}

# Cleanup URL to maximize cache hit.
sub url_cleanup {
    # Remove empty query string parameters.
    # e.g.: www.example.com/index.html?
    if (req.url ~ "\?$") {
        set req.url = regsub(req.url, "\?$", "");
    }

    # Remove port number from host header.
    set req.http.Host = regsub(req.http.Host, ":[0-9]+", "");

    # Remove tracking query string parameters used by analytics tools.
    if (req.url ~ "(\?|&)(cof|cx|gclid|ie|siteurl|utm_campaign|utm_content|utm_medium|utm_source|utm_term)=") {
        set req.url = regsuball(req.url, "&(cof|cx|gclid|ie|siteurl|utm_campaign|utm_content|utm_medium|utm_source|utm_term)=([A-z0-9_\-\.%25]+)", "");
        set req.url = regsuball(req.url, "\?(cof|cx|gclid|ie|siteurl|utm_campaign|utm_content|utm_medium|utm_source|utm_term)=([A-z0-9_\-\.%25]+)", "?");
        set req.url = regsub(req.url, "\?&", "?");
        set req.url = regsub(req.url, "\?$", "");
    }

    # Sorts query string parameters alphabetically for cache normalization purposes.
    set req.url = std.querysort(req.url);
}
