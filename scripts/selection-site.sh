#!/usr/bin/env bash

PRINTABLE_DRUPAL_SITES_LIST=$DRUPAL_SITES_LIST

usage() {
  printf "selection-site.sh local default\n"
  printf "selection-site.sh dev site_1\n"
  printf "selection-site.sh prod all\n"
  printf "Available sites are: all,%s\n" "${PRINTABLE_DRUPAL_SITES_LIST}"
}

SITE=$1
IFS=',' read -ra DRUPAL_SITES_LIST <<< "$DRUPAL_SITES_LIST"
IFS=',' read -ra DRUPAL_SITES_LIST_FOR_SELECTION <<< "all,$PRINTABLE_DRUPAL_SITES_LIST"

# Check that the argument is there.
if [ -z "$SITE" ]; then
  echo -e "Select the website:\n"
  PS3="Please enter a number: "
  select SITE_SELECTION in "${DRUPAL_SITES_LIST_FOR_SELECTION[@]}"
  do
    if [ -n "${SITE_SELECTION}" ]; then
      SITE="$SITE_SELECTION"
      break
    else
      echo -e "${COLOR_LIGHT_RED}Invalid input, please select between 1 and ${#DRUPAL_SITES_LIST_FOR_SELECTION[@]}${COLOR_NC}"
    fi
  done
fi

if [ "$SITE" != "all" ]; then
  # Check that the selected site is among the available sites.
  FOUND=''
  for DRUPAL_SITE_SELECTION in "${DRUPAL_SITES_LIST[@]}"
  do
    if [ "${DRUPAL_SITE_SELECTION}" == "$SITE" ]; then
      DRUPAL_SITES_LIST=("$SITE")
      FOUND='yes'
    fi
  done

  if [ "${FOUND}" != "yes" ]; then
    echo "The selected site is not available."
    usage
    exit 1
  fi
fi
