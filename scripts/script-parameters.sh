#!/usr/bin/env bash

if [ -z "$1" ]; then
  # shellcheck source=scripts/selection-environment.sh
  . "$(dirname "${BASH_SOURCE[0]}")"/selection-environment.sh local
else
  # shellcheck source=scripts/selection-environment.sh
  . "$(dirname "${BASH_SOURCE[0]}")"/selection-environment.sh "$1"
fi

# Test if jq exists so script-parameters.sh can be included when launched from
# host machine.
if command -v jq &> /dev/null; then
  # Export BEHAT_PARAMS dynamically.
  BEHAT_PARAMS=$(jq -n \
                 --arg APP_PATH "$APP_PATH" \
                 '{"extensions":{"Drupal\\DrupalExtension":{"drupal":{"drupal_root":$APP_PATH}}}}')
  export BEHAT_PARAMS
fi

# shellcheck disable=SC2034
CURRENT_PATH=$(pwd)
# shellcheck disable=SC2034
CURRENT_DATE=$(date "+%Y-%m-%d-%Hh%Mm%Ss")

# shellcheck disable=SC2034
DEVELOPMENT_MODULES=(
#  blazy_ui
#  config_devel
  config_inspector
  dblog
  devel
  devel_a11y
  devel_generate
  devel_php
  field_ui
#  purge_ui
#  renderviz
  speedboxes
#  splide_ui
  update
  upgrade_status
  views_ui
)

# shellcheck disable=SC2034
OPTIONAL_MODULES=(
  # As in this template we do not use custom install profile, enable some
  # modules.
  datetime_range
  inline_form_errors
  media
  media_library
  navigation
  navigation_extra_tools
#  navigation_top_bar
  responsive_image
  syslog
  telephone
  health_check
#  purge
#  purge_drush
#  purge_queuer_coretags
#  purge_processor_cron
  redis
#  search_api
#  search_api_solr
#  varnish_purger
#  varnish_purge_tags
)

# shellcheck disable=SC2034
OPTIONAL_THEMES=(
#  stark
)

# shellcheck disable=SC2034
UNINSTALLED_MODULES=(
  # As in this template we do not use custom install profile, uninstall some
  # modules.
  announcements_feed
  automated_cron
#  comment
#  contact
  history
#  search
#  shortcut
  toolbar
)

# shellcheck disable=SC2034
FILES_EXCLUDED_FROM_PACKAGE=(
#  app/themes/custom/my_theme/assets/images/source
#  app/themes/custom/my_theme/assets/scss
  app/core/modules/*/tests
  app/core/modules/*/modules/*/tests
  app/core/profiles/demo_umami
  app/core/profiles/nightwatch_a11y_testing
  app/core/profiles/nightwatch_testing
#  app/core/profiles/standard
  app/core/profiles/testing
  app/core/profiles/testing_config_import
  app/core/profiles/testing_config_overrides
  app/core/profiles/testing_install_profile_all_dependencies
  app/core/profiles/testing_install_profile_dependencies
  app/core/profiles/testing_missing_dependencies
  app/core/profiles/testing_multilingual
  app/core/profiles/testing_multilingual_with_english
  app/core/profiles/testing_requirements
  app/core/profiles/testing_site_config
  app/core/profiles/testing_themes_blocks
  app/core/profiles/test_language_negotiation
  app/core/tests
  app/core/themes/claro/tests
#  app/core/themes/olivero
  app/core/themes/stable9
  app/core/themes/stark
  app/core/themes/starterkit_theme
  app/core/CHANGELOG.txt
  app/core/COPYRIGHT.txt
  app/core/core.api.php
  app/core/.cspell.json
  app/core/.deprecation-ignore.txt
  app/core/.env.example
  app/core/.eslintignore
  app/core/.eslintrc.jquery.json
  app/core/.eslintrc.json
  app/core/.eslintrc.legacy.json
  app/core/.eslintrc.passing.json
  app/core/.gitignore
  app/core/globals.api.php
  app/core/INSTALL.mysql.txt
  app/core/INSTALL.pgsql.txt
  app/core/INSTALL.sqlite.txt
  app/core/INSTALL.txt
  app/core/LICENSE.txt
  app/core/MAINTAINERS.txt
  app/core/package.json
  app/core/phpcs.xml.dist
  app/core/.phpstan-baseline.php
  app/core/phpstan.neon.dist
  app/core/phpstan-partial.neon
  app/core/phpunit.xml.dist
  app/core/.prettierignore
  app/core/.prettierrc.json
  app/core/.stylelintrc.json
  app/core/UPDATE.txt
  app/core/USAGE.txt
  app/core/yarn.lock
  app/modules/contrib/*/tests
  app/modules/contrib/*/modules/*/tests
  app/modules/custom/*/tests
  app/modules/custom/*/modules/*/tests
  app/sites/development.services.yml
  app/sites/example.settings.local.php
  app/sites/example.sites.php
  app/themes/contrib/*/tests
  app/themes/custom/*/tests
  app/.eslintignore
  app/.ht.router.php
  backups
  conf/drupal/example.sites.php
  conf/env/example.composer.env
  docker
  docs
  drush/README.md
  scripts/assets
  scripts/quality
  scripts/tests
  scripts/contrib-sync.sh
  scripts/contrib-update.sh
  scripts/deploy-package.sh
  scripts/init.sh
  scripts/package-composer-install.sh
  scripts/package-finalize.sh
  .editorconfig
  .git
  .gitattributes
  .gitignore
  .gitlab-ci.yml
  docker-compose.yml
  example.dev.env
  example.gitlab.env
  example.integ.env
  example.preprod.env
  example.prod.env
  example.staging.env
  README.md
)

# shellcheck disable=SC2034
FILES_EXCLUDED_FROM_DOCKER_SYNC=(
  /backups
# Once the project is initialized, a real Github Token should be versioned in
# the example.composer.env file.
#  /conf/env/composer.env
  /data
)

# shellcheck disable=SC2034
PUBLIC_FILES_EXCLUDED_FROM_BACKUP=(
  css
  js
  languages
  matomo
  php
  styles
)

# shellcheck disable=SC2034
PRIVATE_FILES_EXCLUDED_FROM_BACKUP=(
  styles
)

# shellcheck disable=SC2034
ENV_VARIABLES_WITH_SECRETS=(
  DRUPAL_SITE_DEFAULT_ACCOUNT_PASS
  # Only used for Docker environment.
#  DRUPAL_SITE_DEFAULT_DB_ROOT_PASSWORD
  DRUPAL_SITE_DEFAULT_DB_PASSWORD
)

# shellcheck source=scripts/secrets-replacement.sh
. "$(dirname "${BASH_SOURCE[0]}")"/secrets-replacement.sh
