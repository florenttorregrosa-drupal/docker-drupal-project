#!/usr/bin/env bash

CURRENT_SITE_DRUSH_ALIAS="DRUPAL_SITE_${DRUPAL_SITE^^}_DRUSH_ALIAS"

if [ "${DRUPAL_UNINSTALL_MODULES}" = "yes" ]; then
  echo -e "${COLOR_LIGHT_GREEN}${DRUPAL_SITE}: Uninstall modules.${COLOR_NC}"
  MODULES=''
  # shellcheck disable=2153
  for UNINSTALLED_MODULE in "${UNINSTALLED_MODULES[@]}"
  do
    MODULES="${MODULES} ${UNINSTALLED_MODULE}"
  done
  # shellcheck disable=2086
  # Avoid double quotes around $MODULES because we specifically wants word
  # splitting.
  $DRUSH "${!CURRENT_SITE_DRUSH_ALIAS}" pm:uninstall ${MODULES} -y
fi
