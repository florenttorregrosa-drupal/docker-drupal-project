#!/usr/bin/env bash

CURRENT_SITE_DRUSH_ALIAS="DRUPAL_SITE_${DRUPAL_SITE^^}_DRUSH_ALIAS"

if [ "${DRUPAL_INSTALL_OPTIONAL_THEMES}" = "yes" ]; then
  echo -e "${COLOR_LIGHT_GREEN}${DRUPAL_SITE}: Install optional themes.${COLOR_NC}"
  THEMES=''
  # shellcheck disable=2153
  for OPTIONAL_THEME in "${OPTIONAL_THEMES[@]}"
  do
    THEMES="${THEMES} ${OPTIONAL_THEME}"
  done
  THEMES="${THEMES} ${DRUPAL_THEME_ADMIN} ${DRUPAL_THEME_DEFAULT}"

  # shellcheck disable=2086
  # Avoid double quotes around $THEMES because we specifically wants word
  # splitting.
  $DRUSH "${!CURRENT_SITE_DRUSH_ALIAS}" theme:install ${THEMES} -y

  echo -e "${COLOR_LIGHT_GREEN}${DRUPAL_SITE}: Set admin theme.${COLOR_NC}"
  $DRUSH "${!CURRENT_SITE_DRUSH_ALIAS}" config:set system.theme admin "${DRUPAL_THEME_ADMIN}" -y

  echo -e "${COLOR_LIGHT_GREEN}${DRUPAL_SITE}: Set default theme.${COLOR_NC}"
  $DRUSH "${!CURRENT_SITE_DRUSH_ALIAS}" config:set system.theme default "${DRUPAL_THEME_DEFAULT}" -y
fi
