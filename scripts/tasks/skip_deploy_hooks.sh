#!/usr/bin/env bash

CURRENT_SITE_DRUSH_ALIAS="DRUPAL_SITE_${DRUPAL_SITE^^}_DRUSH_ALIAS"

echo -e "${COLOR_LIGHT_GREEN}${DRUPAL_SITE}: Mark deploy hooks as completed.${COLOR_NC}"
$DRUSH "${!CURRENT_SITE_DRUSH_ALIAS}" deploy:mark-complete -y
