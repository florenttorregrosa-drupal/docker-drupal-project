<?php

// phpcs:ignoreFile

declare(strict_types=1);

use DrupalFinder\DrupalFinderComposerRuntime;
use DrupalRector\Set\Drupal10SetList;
use DrupalRector\Set\Drupal8SetList;
use DrupalRector\Set\Drupal9SetList;
use Rector\Config\RectorConfig;
use Rector\PHPUnit\PHPUnit60\Rector\ClassMethod\AddDoesNotPerformAssertionToNonAssertingTestRector;

return static function (RectorConfig $rectorConfig): void {
  $rectorConfig->sets([
    Drupal8SetList::DRUPAL_8,
    Drupal9SetList::DRUPAL_9,
    Drupal10SetList::DRUPAL_10,
  ]);

  $drupalFinder = new DrupalFinderComposerRuntime();
  $drupalRoot = $drupalFinder->getDrupalRoot();
  $rectorConfig->autoloadPaths([
    $drupalRoot . '/core',
    $drupalRoot . '/modules',
    $drupalRoot . '/profiles',
    $drupalRoot . '/themes',
  ]);

  $rectorConfig->skip([
    AddDoesNotPerformAssertionToNonAssertingTestRector::class,
    // This path is used by the upgrade_status module.
    '*/upgrade_status/tests/modules/*',
    // If you would like to skip test directories, uncomment the following lines:
    // '*/tests/*',
    // '*/Tests/*',
  ]);

  $rectorConfig->fileExtensions([
    'engine',
    'inc',
    'install',
    'module',
    'php',
    'profile',
    'theme',
  ]);

  // Create `use` statements.
  $rectorConfig->importNames(FALSE, FALSE);
  // Do not convert `\Drupal` to `Drupal`, etc.
  $rectorConfig->importShortClasses(FALSE);
};
