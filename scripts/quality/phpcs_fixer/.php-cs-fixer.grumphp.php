<?php

declare(strict_types=1);

use drupol\PhpCsFixerConfigsDrupal\Config\Drupal8;

$custom_config = new Drupal8();

$custom_config->setCacheFile('/tmp/.php_cs.cache');

// Overrides rules provided by vendor.
$rules = $custom_config->getRules();

// @see https://cs.symfony.com/doc/rules/index.html.
$rules['blank_line_before_statement']['statements'] = [
  'case',
  'declare',
  'default',
];
$rules['declare_strict_types'] = TRUE;
$rules['doctrine_annotation_array_assignment']['operator'] = '=';
$rules['doctrine_annotation_spaces']['before_argument_assignments'] = TRUE;
$rules['doctrine_annotation_spaces']['after_argument_assignments'] = TRUE;
$rules['doctrine_annotation_spaces']['before_array_assignments_equals'] = TRUE;
$rules['doctrine_annotation_spaces']['after_array_assignments_equals'] = TRUE;
$rules['error_suppression'] = FALSE;
$rules['fully_qualified_strict_types']['leading_backslash_in_global_namespace'] = TRUE;
$rules['general_phpdoc_tag_rename'] = [
  'case_sensitive' => TRUE,
  'fix_annotation' => TRUE,
  'fix_inline' => TRUE,
  'replacements' => [
    'inheritDoc' => 'inheritdoc',
  ],
];
$rules['native_function_invocation']['include'] = [
  '@all',
];
$rules['native_function_invocation']['scope'] = 'all';
$rules['no_superfluous_phpdoc_tags'] = FALSE;
$rules['ordered_class_elements'] = FALSE;
$rules['php_unit_internal_class'] = FALSE;
$rules['php_unit_test_case_static_method_calls']['call_type'] = 'this';
$rules['php_unit_test_class_requires_covers'] = FALSE;
$rules['strict_comparison'] = FALSE;
$rules['trailing_comma_in_multiline']['elements'][] = 'parameters';

$custom_config->setRules($rules);

return $custom_config;
