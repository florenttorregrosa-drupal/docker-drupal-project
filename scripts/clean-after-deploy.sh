#!/usr/bin/env bash
# shellcheck disable=SC2029

usage() {
  printf "clean-after-deploy.sh [integ|staging|preprod|prod]\n"
}

environment=''

if [ -n "$1" ]; then
  environment=$1
fi

SUDO="sudo -u ${WEBSERVER_USER}"

# Check that all required parameters are present.
if [ -z "${environment}" ]; then
  echo "Missing target environment parameter."
  usage
  exit 1
elif [ "${environment}" = "local" ]; then
  echo "This script is not meant to be launched for local environment."
  usage
  exit 1
fi

# shellcheck source=scripts/script-parameters.sh
. "$(dirname "${BASH_SOURCE[0]}")"/script-parameters.sh "${environment}"

if [ -z "${CLEAN_RELEASES_NUMBER}" ]; then
  echo "The CLEAN_RELEASES_NUMBER is missing."
  usage
  exit 1
fi
if (( CLEAN_RELEASES_NUMBER < 1 )); then
  echo "The CLEAN_RELEASES_NUMBER must be higher than 1."
  usage
  exit 1
fi

IFS=',' read -ra FRONT_SERVERS_HOSTS <<< "$FRONT_SERVERS_HOSTS"
# shellcheck disable=SC2128
IFS=',' read -ra DRUPAL_SITES_LIST <<< "$DRUPAL_SITES_LIST"

# Loop on each server to clean the releases folder (keep only X latest).
for FRONT_SERVERS_HOST in "${FRONT_SERVERS_HOSTS[@]}"
do
  echo -e "${COLOR_LIGHT_GREEN}Server ${FRONT_SERVERS_HOST}: Delete old releases (keep ${CLEAN_RELEASES_NUMBER}).${COLOR_NC}"
  ssh "${SSH_USER}"@"${FRONT_SERVERS_HOST}" '
  cd '"${DEPLOYMENT_PATH}"'/releases
  RELEASES=$(ls -dt * | awk NR\>'"${CLEAN_RELEASES_NUMBER}"')

  if [ -z "${RELEASES}" ]; then
    echo "Nothing to delete!"
  else
    can_delete=true

    for RELEASE in $RELEASES
    do
      if [ -n "{$RELEASE}" ]; then
        for website in '"${DRUPAL_SITES_LIST[*]}"'
        do
          if [ $(readlink -- "'"${DEPLOYMENT_PATH}"'"/sites/$website/current) = "'"${DEPLOYMENT_PATH}"'"/releases/$RELEASE ]; then
              echo -e "'"${COLOR_BROWN_ORANGE}"' ${RELEASE}: Cannot delete release (used by: $website).'"${COLOR_NC}"'"
              can_delete=false
              continue 2
          fi
        done
      fi

      if [ $can_delete ]; then
          '"${SUDO}"' rm -rf "'"${DEPLOYMENT_PATH}"'"/releases/$RELEASE
          echo -e "'"${COLOR_BLUE}"' ${RELEASE}: Release deleted.'"${COLOR_NC}"'"
      fi
    done
  fi
  '
done

for DRUPAL_SITE in "${DRUPAL_SITES_LIST[@]}"
do
  FOLDER_NAME="DRUPAL_SITE_${DRUPAL_SITE^^}_FOLDER_NAME"
  echo -e "${COLOR_LIGHT_GREEN}Server ${FRONT_MAIN_SERVER_HOST}: ${DRUPAL_SITE}: Delete old backups  (keep ${CLEAN_BACKUPS_NUMBER}).${COLOR_NC}"
  $SSH "${SSH_USER}"@"${FRONT_MAIN_SERVER_HOST}" "cd ${DEPLOYMENT_PATH}/backups/${!FOLDER_NAME}/ && ls -t | awk 'NR>${CLEAN_BACKUPS_NUMBER}' | tr '\n' '\0' | xargs -0 rm -rf"
done
